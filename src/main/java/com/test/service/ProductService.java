package com.test.service;

import java.util.List;

import com.test.model.Product;

public interface ProductService {

	Product save(Product product);

	List<Product> getList();
	
	List<Product> getProductByType(String productType);
	
	String getProductPriceById(String productId);

	String delete(String productId);
}
