package com.test.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.hazelcast.core.HazelcastInstance;
import com.test.exception.ProductAlreadyExistsException;
import com.test.model.Product;
import com.test.repository.ProductRepository;


@Service
@Validated
public class ProductServiceImpl implements ProductService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);
	private final ProductRepository repository;


	@Inject
	public ProductServiceImpl(final ProductRepository repository) {
		this.repository = repository;
	}

    private final HazelcastInstance hazelcastInstance = null;
    
	@Transactional
	public Product save(Product product) {
		LOGGER.debug("Creating {}", product);
		Product existing = repository.findOne(product.getId());
		if (existing != null) {
			throw new ProductAlreadyExistsException(String.format(
					"There already exists a product with id=%s",
					product.getId()));
		}
		return repository.save(product);
	}

	@Transactional(readOnly = true)
	public List<Product> getList() {
		LOGGER.debug("Retrieving the list of all Products");
		return repository.findAll();
	}
	
	@Transactional(readOnly = true)
	public Product getProduct(String productId) {
		LOGGER.debug("Retrieving the product by id {}",productId);
		return repository.findOne(productId);
	}
	
	@Transactional(readOnly = true)
	public List<Product> getProductByType(String productType) {
		LOGGER.debug("Retrieving the product by type {}",productType);
		List<Product> products = new ArrayList<Product>();
		List<Product> allProducts =  repository.findAll();
		if(products!=null){
			for (Product product : allProducts) {
				if(product.getType().equals(productType)){
					products.add(product);
				}
			}	
		}
		return products;
	}
	
	
	

	@Transactional
	public String getProductPriceById(String productId) {
		LOGGER.debug("Retriving  product price by id {}",productId);
		Product product = getProduct(productId);
		if(product !=null){
			return product.getPrice();
		}else{
			return null;
		}
	}

	
	
	@Transactional
	public String delete(String productId) {
		LOGGER.debug("Deleting the product by id {}",productId);
		if(getProduct(productId) !=null){
			repository.delete(productId);
			return "Product Deleted";
		}else{
			return "Product Not Found"; 
		}
			
	}

}
