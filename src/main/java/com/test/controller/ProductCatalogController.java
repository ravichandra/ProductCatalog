package com.test.controller;

import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.test.exception.ProductAlreadyExistsException;
import com.test.model.Product;
import com.test.service.ProductService;

@RestController
public class ProductCatalogController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductCatalogController.class);
    private final ProductService productService;
    
    @Inject
    public ProductCatalogController(final ProductService productService) {
        this.productService = productService;
    }
    
    /*
   @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test() {
        System.out.println("Working");
        return "Working";
    }*/
   
   @ApiOperation(value = "products", nickname = "products")
   @RequestMapping(value = "/create", method = RequestMethod.POST)
   public Product createProduct(@RequestBody @Valid final Product product) {
	   LOGGER.debug("Received request to create the {}", product);
       return productService.save(product);
   }
   
   @ApiOperation(value = "products", nickname = "products")
   @RequestMapping(value = "/delete/{productId}", method = RequestMethod.DELETE)
   public String deleteProductById(@PathVariable @Valid final String productId) {
	   LOGGER.debug("Received request to delete product with id {}", productId);
	   return productService.delete(productId);   
   }
   
   @ApiOperation(value = "products", nickname = "products")
   @RequestMapping(value = "/product/{productType}", method = RequestMethod.GET)
   public List<Product> deleteProduct(@PathVariable @Valid final String productType) {
	   LOGGER.debug("Received request to list all products with type {}",productType);
	   return productService.getProductByType(productType);   
   }
   
   @ApiOperation(value = "products", nickname = "products")
   @RequestMapping(value = "/productprice/{productId}", method = RequestMethod.GET)
   public String getProductPriceById(@PathVariable @Valid final String productId) {
	   LOGGER.debug("Received request to get price of product ",productId);
	   return productService.getProductPriceById(productId)  ; 
   }
   
   @ApiOperation(value = "products", nickname = "products")
   @RequestMapping(value = "/products", method = RequestMethod.GET)
   public List<Product> listProducts() {
	   LOGGER.debug("Received request to list all products");
       return productService.getList();
   }
   
   @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleUserAlreadyExistsException(ProductAlreadyExistsException e) {
        return e.getMessage();
    }

}
