package com.test.model;

import io.swagger.annotations.ApiModelProperty;

import com.google.common.base.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Product {

    @Id
    @NotNull
    @Size(max = 64)
    @Column(name = "id", nullable = false, updatable = false)
    private String id;

    @NotNull
    @Size(max = 64)
    @Column(name = "name", nullable = false)
    @ApiModelProperty(notes = "The name of the product", required = true)
    private String name;
    
    @NotNull
    @Size(max = 64)
    @Column(name = "type", nullable = false)
    private String type;

    @NotNull
    @Size(max = 64)
    @Column(name = "price", nullable = false)
    private String price;
    
    Product() {
    }

    public Product(final String id, final String name,final String type,final String price) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	@Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("type", type)
                .add("price", price)
                .toString();
    }
}
