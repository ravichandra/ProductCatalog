package com.test.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.test.model.Product;
import com.test.service.ProductService;
import com.test.util.ProductUtil;


@RunWith(MockitoJUnitRunner.class)
public class ProductCatalogControllerTest {

	@Mock
	private ProductService productService;

	private ProductCatalogController productCatalogController;

	@Before
	public void setUp() throws Exception {
		productCatalogController = new ProductCatalogController(productService);
	}
	
	@Test
    public void shouldCreateProduct() throws Exception {
        final Product savedProduct = stubServiceToReturnStoredProduct();
        final Product product = ProductUtil.createProduct();
        Product returnedProduct = productCatalogController.createProduct(product);
        // verify product was passed to ProductService
        verify(productService, times(1)).save(product);
        assertEquals("Returned product should come from the service", savedProduct, returnedProduct);
    }

    private Product stubServiceToReturnStoredProduct() {
        final Product product = ProductUtil.createProduct();
        when(productService.save(any(Product.class))).thenReturn(product);
        return product;
    }


    @Test
    public void shouldListAllProducts() throws Exception {
        stubServiceToReturnExistingProducts(10);
        Collection<Product> products = productCatalogController.listProducts();
        assertNotNull(products);
        assertEquals(10, products.size());
        // verify product was passed to ProductService
        verify(productService, times(1)).getList();
    }

    private void stubServiceToReturnExistingProducts(int howMany) {
        when(productService.getList()).thenReturn(ProductUtil.createProductList(howMany));
    }

}
