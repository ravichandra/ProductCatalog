package com.test.service;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.test.exception.ProductAlreadyExistsException;
import com.test.model.Product;
import com.test.repository.ProductRepository;
import com.test.util.ProductUtil;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

    @Mock
    private ProductRepository productRepository;

    private ProductService productService;

    @Before
    public void setUp() throws Exception {
        productService = new ProductServiceImpl(productRepository);
    }

    @Test
    public void shouldSaveNewProduct_GivenThereDoesNotExistOneWithTheSameId_ThenTheSavedProductShouldBeReturned() throws Exception {
        final Product savedProduct = stubRepositoryToReturnProductOnSave();
        final Product product = ProductUtil.createProduct();
        final Product returnedProduct = productService.save(product);
        // verify repository was called with product
        verify(productRepository, times(1)).save(product);
        assertEquals("Returned product should come from the repository", savedProduct, returnedProduct);
    }

    private Product stubRepositoryToReturnProductOnSave() {
        Product product = ProductUtil.createProduct();
        when(productRepository.save(any(Product.class))).thenReturn(product);
        return product;
    }

    @Test
    public void shouldSaveNewProduct_GivenThereExistsOneWithTheSameId_ThenTheExceptionShouldBeThrown() throws Exception {
        stubRepositoryToReturnExistingProduct();
        try {
            productService.save(ProductUtil.createProduct());
            fail("Expected exception");
        } catch (ProductAlreadyExistsException ignored) {
        }
        verify(productRepository, never()).save(any(Product.class));
    }

    private void stubRepositoryToReturnExistingProduct() {
        final Product product = ProductUtil.createProduct();
        when(productRepository.findOne(product.getId())).thenReturn(product);
    }

    @Test
    public void shouldListAllProducts_GivenThereExistSome_ThenTheCollectionShouldBeReturned() throws Exception {
        stubRepositoryToReturnExistingProducts(10);
        Collection<Product> list = productService.getList();
        assertNotNull(list);
        assertEquals(10, list.size());
        verify(productRepository, times(1)).findAll();
    }

    private void stubRepositoryToReturnExistingProducts(int howMany) {
        when(productRepository.findAll()).thenReturn(ProductUtil.createProductList(howMany));
    }

    @Test
    public void shouldListAllProducts_GivenThereNoneExist_ThenTheEmptyCollectionShouldBeReturned() throws Exception {
        stubRepositoryToReturnExistingProducts(0);
        Collection<Product> list = productService.getList();
        assertNotNull(list);
        assertTrue(list.isEmpty());
        verify(productRepository, times(1)).findAll();
    }

}
