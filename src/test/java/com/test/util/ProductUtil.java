package com.test.util;


import java.util.ArrayList;
import java.util.List;

import com.test.model.Product;

public class ProductUtil {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String TYPE = "name";
    private static final String PRICE =  "100";

    private ProductUtil() {
    }

    public static Product createProduct() {
        return new Product(ID, NAME,TYPE,PRICE);
    }

    public static List<Product> createProductList(int number) {
        List<Product> productList = new ArrayList<>();
        for (int i = 0; i < number; i++) {
        	productList.add(new Product(ID + "#" + i, NAME,TYPE,PRICE));
        }
        return productList;
    }

}
